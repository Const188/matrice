﻿using System;

namespace MatriceTabe
{
    class MatriceTab
    {

        public int [,] matrix;
        public int size;

         public MatriceTab (int [,] matrix) {   //Prend en parametre un tableau à
            this.matrix = matrix;
            //this.size = sqmatrix.Length;
            this.size = matrix.GetLength(0);
         }

         public int GetSize () {  //Prend en parametre et un entier
            return this.size;
         }
         
          public string ToString() { //Permet d'afficher la matrice
            string ret = "";
            for (int i = 0; i < this.size; i++) {
                //Console.Write("| ");
                ret = ret + "| ";
                for (int j = 0; j < this.size; j++) {
                    //Console.Write(test[i,j] + " | ");
                    ret = ret + this.matrix[i,j] + " | ";
                }
                //Console.WriteLine();
                ret = ret + "\n";
            }
            return ret;
        }

        public void Set(int x, int y, int val) {
            this.matrix[x,y] = val;
        }
        public static string test () {  //Permet d'afficher TestMatriceTab
            return " ";
        }
    }
}