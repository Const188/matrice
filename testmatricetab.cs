﻿using System;

namespace MatriceTabe
{
    class TestMatriceTab2
    {
        static void Main(string[] args) //Fonction Main qui retourne un string
        {

            int [, ] test = { {1,2,3 }, {4,5,6}, {8, 9, 10} };  //On rentre les valeurs qui seront dans la matrice
            MatriceTab sq = new MatriceTab(test);
            Console.WriteLine(sq.ToString()); //Affichage de la matrice

        }
    }
}